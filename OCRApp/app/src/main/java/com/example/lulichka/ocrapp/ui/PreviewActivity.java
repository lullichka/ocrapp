package com.example.lulichka.ocrapp.ui;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.lulichka.ocrapp.R;
import com.example.lulichka.ocrapp.data.Database;
import com.example.lulichka.ocrapp.data.LogsContentProvider;
import com.squareup.picasso.Picasso;

public class PreviewActivity extends AppCompatActivity
{
  private static final String PHOTO_PATH = "photo_path";
  private static final String NUMBER_VALUE = "number";
  private static final String URI_VALUE = "value_uri";
  private static final String PREFIX = "prefix";
  private static final String PREFS_NAME = "prefs";
  private String mPhotoPath;
  private String mNumData;
  private String mUriString;
  private TextView tvEntry;
  private String TAG = PreviewActivity.class.getSimpleName();

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_preview);
    ImageView imageView = (ImageView) findViewById(R.id.ivPreviewEntry);
    tvEntry = (TextView) findViewById(R.id.tvEntry);
    if (getIntent().getStringExtra(PHOTO_PATH) != null && getIntent().getStringExtra(NUMBER_VALUE) != null){
      mPhotoPath = getIntent().getStringExtra(PHOTO_PATH);
      mNumData = getIntent().getStringExtra(NUMBER_VALUE);
      mUriString = getIntent().getStringExtra(URI_VALUE);
      tvEntry.setText(mNumData);
      Log.d(TAG, mPhotoPath);
      Picasso.with(this).load("file://" + mPhotoPath).fit()
              .placeholder(R.drawable.placeholder).centerCrop().into(imageView);
    } else {
      TextView tvWrong = (TextView) findViewById(R.id.tvWrong);
      tvWrong.setVisibility(View.VISIBLE);
    }
  }

  public void editEntry(View view)
  {
    Intent intent = new Intent(PreviewActivity.this, EditEntryActivity.class);
    intent.putExtra(NUMBER_VALUE, mNumData);
    //intent.putExtra(URI_VALUE, mUriString);
    startActivity(intent);
  }

  public void copyEntry(View view)
  {
    ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
    ClipData clip = ClipData.newPlainText("number", mNumData);
    clipboard.setPrimaryClip(clip);
    Toast.makeText(this, "Entry copied to clipboard!", Toast.LENGTH_SHORT).show();
  }

  public void useEntry(View view)
  {
    SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
    String prefix = sharedPreferences.getString(PREFIX, null);
    String phone = prefix + mNumData;
    Log.d(TAG, phone);
    Intent phoneIntent = new Intent(Intent.ACTION_DIAL, Uri.fromParts(
            "tel", phone, null));
    startActivity(phoneIntent);
  }

  public void saveEntry(View view)
  {
    ContentValues values = new ContentValues();
    values.put(Database.COLUMN_NUMBER, mNumData);
    final Uri uri = getApplicationContext().getContentResolver().insert(
            LogsContentProvider.CONTENT_URI, values);
    Log.d(TAG, "inserted " + uri.getPath());
  }
}
