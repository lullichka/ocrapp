package com.example.lulichka.ocrapp;


import android.content.Context;
import android.database.Cursor;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class LogsAdapter extends RecyclerView.Adapter<LogsAdapter.LogsViewHolder>
{
  static final String COLUMN_ID = "_id";
  static final String COLUMN_NUMBER = "number";
  private Context mContext;
  private Cursor mCursor;

  public LogsAdapter(Context context, Cursor cursor)
  {
    mCursor = cursor;
    mContext = context;
  }

  public Cursor swapCursor(Cursor cursor)
  {
    if (mCursor == cursor) {
      return null;
    }
    Cursor oldCursor = mCursor;
    this.mCursor = cursor;
    if (cursor != null) {
      this.notifyDataSetChanged();
    }
    return oldCursor;
  }

  @Override
  public LogsViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
  {
    Context context = parent.getContext();
    LayoutInflater inflater = LayoutInflater.from(context);
    View view = inflater.inflate(R.layout.log_adapter_view_item, parent, false);
    return new LogsViewHolder(view);
  }

  @Override
  public void onBindViewHolder(LogsViewHolder holder, int position)
  {
    mCursor.moveToPosition(position);
    holder.setContextAndCursor(mContext, mCursor);
    String number = mCursor.getString(mCursor.getColumnIndex(COLUMN_NUMBER));
    holder.tvNumber.setText(number);
  }


  @Override
  public int getItemCount()
  {
    return (mCursor == null) ? 0 : mCursor.getCount();
  }

  static class LogsViewHolder extends RecyclerView.ViewHolder
  {
    Context context;
    Cursor cursor;
    TextView tvNumber;


    private LogsViewHolder setContextAndCursor(Context context, Cursor cursor)
    {
      this.cursor = cursor;
      this.context = context;
      return this;
    }

    private LogsViewHolder(View itemView)
    {
      super(itemView);
      tvNumber = (TextView) itemView.findViewById(R.id.tvItem);
    }
  }
}

