package com.example.lulichka.ocrapp.data;


import android.content.ContentProvider;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.text.TextUtils;

import java.util.Arrays;
import java.util.HashSet;

public class LogsContentProvider extends ContentProvider
{
  private NumbersSQLiteOpenHelper mDatabase;

  private static final int RESULTS = 10;
  private static final int RESULT_ID = 20;

  private static final String AUTHORITY = "com.example.lulichka.ocrapp";

  private static final String BASE_PATH = "numbers";
  public static final Uri CONTENT_URI = Uri.parse("content://" + AUTHORITY
          + "/" + BASE_PATH);

  public static final String CONTENT_TYPE = ContentResolver.CURSOR_DIR_BASE_TYPE
          + "/numbers";
  public static final String CONTENT_ITEM_TYPE = ContentResolver.CURSOR_ITEM_BASE_TYPE
          + "/number";

  private static final UriMatcher sURIMatcher = new UriMatcher(
          UriMatcher.NO_MATCH);

  static {
    sURIMatcher.addURI(AUTHORITY, BASE_PATH, RESULTS);
    sURIMatcher.addURI(AUTHORITY, BASE_PATH + "/#", RESULT_ID);
  }

  @Override
  public boolean onCreate()
  {
    mDatabase = new NumbersSQLiteOpenHelper(getContext());
    return false;
  }

  @Nullable
  @Override
  public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder)
  {
    SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
    checkColumns(projection);
    queryBuilder.setTables(Database.TABLE_RESULTS);

    int uriType = sURIMatcher.match(uri);
    switch (uriType) {
      case RESULTS:
        break;
      case RESULT_ID:
        queryBuilder.appendWhere(Database.COLUMN_ID + "="
                + uri.getLastPathSegment());
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }

    SQLiteDatabase db = mDatabase.getWritableDatabase();
    Cursor cursor = queryBuilder.query(db, projection, selection,
            selectionArgs, null, null, sortOrder);
    cursor.setNotificationUri(getContext().getContentResolver(), uri);
    return cursor;
  }

  private void checkColumns(String[] projection)
  {
    String[] available = {Database.COLUMN_ID,
            Database.COLUMN_NUMBER};
    if (projection != null) {
      HashSet<String> requestedColumns = new HashSet<>(
              Arrays.asList(projection));
      HashSet<String> availableColumns = new HashSet<>(
              Arrays.asList(available));
      if (!availableColumns.containsAll(requestedColumns)) {
        throw new IllegalArgumentException(
                "Unknown columns in projection");
      }
    }
  }

  @Nullable
  @Override
  public String getType(Uri uri)
  {
    return null;
  }

  @Nullable
  @Override
  public Uri insert(Uri uri, ContentValues contentValues)
  {
    int uriType = sURIMatcher.match(uri);
    SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
    long id = 0;
    switch (uriType) {
      case RESULTS:
        id = sqlDB.insert(Database.TABLE_RESULTS, null, contentValues);
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return Uri.parse(BASE_PATH + "/" + id);
  }

  @Override
  public int delete(Uri uri, String selection, String[] selectionArgs)
  {
    int uriType = sURIMatcher.match(uri);
    SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
    int rowsDeleted = 0;
    switch (uriType) {
      case RESULTS:
        rowsDeleted = sqlDB.delete(Database.TABLE_RESULTS, selection,
                selectionArgs);
        break;
      case RESULT_ID:
        String id = uri.getLastPathSegment();
        if (TextUtils.isEmpty(selection)) {
          rowsDeleted = sqlDB.delete(
                  Database.TABLE_RESULTS,
                  Database.COLUMN_ID + "=" + id,
                  null);
        } else {
          rowsDeleted = sqlDB.delete(
                  Database.TABLE_RESULTS,
                  Database.COLUMN_ID + "=" + id
                          + " and " + selection,
                  selectionArgs);
        }
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return rowsDeleted;
  }

  @Override
  public int update(Uri uri, ContentValues contentValues, String selection,
                    String[] selectionArgs)
  {

    int uriType = sURIMatcher.match(uri);
    SQLiteDatabase sqlDB = mDatabase.getWritableDatabase();
    int rowsUpdated;
    switch (uriType) {
      case RESULTS:
        rowsUpdated = sqlDB.update(Database.TABLE_RESULTS,
                contentValues,
                selection,
                selectionArgs);
        break;
      case RESULT_ID:
        String id = uri.getLastPathSegment();
        if (TextUtils.isEmpty(selection)) {
          rowsUpdated = sqlDB.update(Database.TABLE_RESULTS,
                  contentValues,
                  Database.COLUMN_ID + "=" + id,
                  null);
        } else {
          rowsUpdated = sqlDB.update(Database.TABLE_RESULTS,
                  contentValues,
                  Database.COLUMN_ID + "=" + id
                          + " and "
                          + selection,
                  selectionArgs);
        }
        break;
      default:
        throw new IllegalArgumentException("Unknown URI: " + uri);
    }
    getContext().getContentResolver().notifyChange(uri, null);
    return rowsUpdated;
  }

}
