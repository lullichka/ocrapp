package com.example.lulichka.ocrapp;

import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;

import com.example.lulichka.ocrapp.camera.GraphicOverlay;
import com.google.android.gms.vision.text.TextBlock;


public class OcrGraphic extends GraphicOverlay.Graphic {

  private int mId;

  private static final int TEXT_COLOR = Color.WHITE;
  private static final int BOUND_COLOR = Color.WHITE;

  private static Paint sRectPaint;
  private static Paint sTextPaint;
  private final TextBlock mText;
  private String mWord;

  OcrGraphic(GraphicOverlay overlay, TextBlock text, String word) {
    super(overlay);

    mText = text;
    mWord = word;

    if (sRectPaint == null) {
      sRectPaint = new Paint();
      sRectPaint.setColor(BOUND_COLOR);
      sRectPaint.setStyle(Paint.Style.STROKE);
      sRectPaint.setStrokeWidth(4.0f);
    }

    if (sTextPaint == null) {
      sTextPaint = new Paint();
      sTextPaint.setColor(TEXT_COLOR);
      sTextPaint.setTextSize(70.0f);
    }
    // Redraw the overlay, as this graphic has been added.
    postInvalidate();
  }


  public int getId() {
    return mId;
  }

  public void setId(int id) {
    this.mId = id;
  }

  public TextBlock getTextBlock() {
    return mText;
  }

  /**
   * Checks whether a point is within the bounding box of this graphic.
   * The provided point should be relative to this graphic's containing overlay.
   *
   * @param x An x parameter in the relative context of the canvas.
   * @param y A y parameter in the relative context of the canvas.
   * @return True if the provided point is contained within this graphic's bounding box.
   */
  public boolean contains(float x, float y) {
    TextBlock text = mText;
    if (text == null) {
      return false;
    }
    RectF rect = new RectF(text.getBoundingBox());
    rect.left = translateX(rect.left);
    rect.top = translateY(rect.top);
    rect.right = translateX(rect.right);
    rect.bottom = translateY(rect.bottom);
    return (rect.left < x && rect.right > x && rect.top < y && rect.bottom > y);
  }

  /**
   * Draws the text block annotations for position, size, and raw value on the supplied canvas.
   */
  @Override
  public void draw(Canvas canvas) {
    TextBlock text = mText;
    if (text == null) {
      return;
    }

    // Draws the bounding box around the TextBlock.
    RectF rect = new RectF(text.getBoundingBox());
    rect.left = translateX(rect.left);
    rect.top = translateY(rect.top);
    rect.right = translateX(rect.right);
    rect.bottom = translateY(rect.bottom);
    canvas.drawRect(rect, sRectPaint);

    //Draws serial number inside bounding box
    float left = translateX(text.getBoundingBox().left);
    float bottom = translateY(text.getBoundingBox().bottom);
    canvas.drawText(mWord, left, bottom, sTextPaint);
  }
}