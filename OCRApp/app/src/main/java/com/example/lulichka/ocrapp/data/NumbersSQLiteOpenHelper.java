package com.example.lulichka.ocrapp.data;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class NumbersSQLiteOpenHelper extends SQLiteOpenHelper
{

  private static final String TAG = NumbersSQLiteOpenHelper.class.getSimpleName();

  private static final String DATABASE_NAME = "numbers.db";
  private static final int DATABASE_VERSION = 1;


  NumbersSQLiteOpenHelper(Context context)
  {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
  }

  @Override
  public void onCreate(SQLiteDatabase sqLiteDatabase)
  {
    Database.onCreate(sqLiteDatabase);
    Log.d(TAG, "database created");
  }

  @Override
  public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
  {
    Database.onUpgrade(sqLiteDatabase, oldVersion, newVersion);
  }
}
