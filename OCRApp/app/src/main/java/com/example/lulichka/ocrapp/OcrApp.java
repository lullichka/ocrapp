package com.example.lulichka.ocrapp;

import android.app.Application;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;

/**
 * Created by lullichka on 14.11.17.
 */
@ReportsCrashes(mailTo = "lullichka@gmail.com",
        mode = ReportingInteractionMode.TOAST,
        resToastText = R.string.crash_toast_text)
public class OcrApp extends Application
{
  @Override
  public void onCreate() {
    super.onCreate();
    ACRA.init(this);
  }
}
