package com.example.lulichka.ocrapp.data;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;


public class Database
{
  private static final String TAG = Database.class.getSimpleName();

  static final String TABLE_RESULTS = "numbers";
  static final String COLUMN_ID = "_id";
  public static final String COLUMN_NUMBER = "number";

  private static final String DATABASE_CREATE = "create table "
          + TABLE_RESULTS + "( " + COLUMN_ID
          + " integer primary key autoincrement, "
          + COLUMN_NUMBER
          + " text "
          + ");";

  static void onCreate(SQLiteDatabase database)
  {
    database.execSQL(DATABASE_CREATE);
  }

  static void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion)
  {
    Log.w(TAG,
            "Upgrading database from version " + oldVersion + " to "
                    + newVersion + ", which will destroy all old data");
    sqLiteDatabase.execSQL("DROP TABLE IF EXISTS " + TABLE_RESULTS);
    onCreate(sqLiteDatabase);
  }

}
