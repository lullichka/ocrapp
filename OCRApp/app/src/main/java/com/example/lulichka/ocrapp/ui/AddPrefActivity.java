package com.example.lulichka.ocrapp.ui;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lulichka.ocrapp.R;

public class AddPrefActivity extends AppCompatActivity
{
  private static final String PREFIX = "prefix";
  private static final String PREFS_NAME = "prefs";

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_pref);
    final EditText editText = (EditText) findViewById(R.id.etPrefix);
    Button button = (Button) findViewById(R.id.btnEdit);
    button.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        SharedPreferences sharedPreferences = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(PREFIX, editText.getText().toString());
        editor.apply();
        Toast.makeText(AddPrefActivity.this, "Saved "+editText.getText().toString(), Toast.LENGTH_SHORT).show();
        finish();
      }
    });
  }
}
