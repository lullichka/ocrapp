package com.example.lulichka.ocrapp.ui;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.example.lulichka.ocrapp.LogsAdapter;
import com.example.lulichka.ocrapp.R;
import com.example.lulichka.ocrapp.data.LogsContentProvider;

public class ListActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>
{
  static final String COLUMN_ID = "_id";
  static final String COLUMN_NUMBER = "number";
  private static final String TAG = ListActivity.class.getSimpleName();
  private RecyclerView mRecyclerView;
  private LinearLayoutManager mLayoutManager;
  private LogsAdapter mAdapter;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_list);
    getSupportLoaderManager().initLoader(0, null, this);
    mRecyclerView = (RecyclerView) findViewById(R.id.rv_items);
    mRecyclerView.setHasFixedSize(true);
    mLayoutManager = new LinearLayoutManager(this);
    mRecyclerView.setLayoutManager(mLayoutManager);
    mAdapter = new LogsAdapter(this, null);
    mRecyclerView.setAdapter(mAdapter);
  }

  @Override
  public Loader<Cursor> onCreateLoader(int id, Bundle args)
  {
    String[] projection = {COLUMN_ID, COLUMN_NUMBER};
    return new CursorLoader(this,
            LogsContentProvider.CONTENT_URI, projection, null, null, null);
  }

  @Override
  public void onLoadFinished(Loader<Cursor> loader, Cursor data)
  {
    if (data.getCount() != 0) {
      TextView tvNoLogs = (TextView) findViewById(R.id.tv_nologs);
      tvNoLogs.setVisibility(View.INVISIBLE);
    }
    Log.d(TAG, String.valueOf(data.getCount()));
    mAdapter.swapCursor(data);
  }

  @Override
  public void onLoaderReset(Loader<Cursor> loader)
  {
    mAdapter.swapCursor(null);
  }
}
