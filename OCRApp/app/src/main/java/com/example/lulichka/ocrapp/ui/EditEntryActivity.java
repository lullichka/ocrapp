package com.example.lulichka.ocrapp.ui;

import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.lulichka.ocrapp.R;
import com.example.lulichka.ocrapp.data.Database;
import com.example.lulichka.ocrapp.data.LogsContentProvider;

public class EditEntryActivity extends AppCompatActivity
{
  private static final String NUMBER_VALUE = "number";
  private static final String URI_VALUE = "value_uri";
  private static final String TAG = EditEntryActivity.class.getSimpleName();
  private String mNumData;
  private String mUriString;

  @Override
  protected void onCreate(Bundle savedInstanceState)
  {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_edit_entry);
    final EditText editText = (EditText) findViewById(R.id.etEntry);
    Button button = (Button) findViewById(R.id.btnEdit);
    if (getIntent().getStringExtra(NUMBER_VALUE) != null) {
      mNumData = getIntent().getStringExtra(NUMBER_VALUE);
      editText.setText(mNumData);
      //mUriString = getIntent().getStringExtra(URI_VALUE);
    }
    button.setOnClickListener(new View.OnClickListener()
    {
      @Override
      public void onClick(View v)
      {
        ContentValues values = new ContentValues();
        values.put(Database.COLUMN_NUMBER, editText.getText().toString());
        final Uri uri = getApplicationContext().getContentResolver().insert(
                LogsContentProvider.CONTENT_URI, values);
        Log.d(TAG, "inserted " + uri.getPath());
        Toast.makeText(EditEntryActivity.this, "Edited", Toast.LENGTH_SHORT).show();
        finish();
      }
    });
  }
}
